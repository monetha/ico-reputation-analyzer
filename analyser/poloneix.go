package analyser

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/monetha/ico-reputation-analyzer/types"
)

const poloneixURL = "https://poloniex.com/public?command=returnChartData&currencyPair=USDT_ETH&start=%d&end=%d&period=86400"

func getEthRateFromPoloneix(startDate, endDate int64) (startDateRate float64, endDateRate float64, maxEthRate float64, err error) {
	resp, err := http.Get(fmt.Sprintf(poloneixURL, startDate, endDate))
	if err != nil {
		return
	}

	poloneixData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	var ethRatesData []types.PoloneixData
	err = json.Unmarshal(poloneixData, &ethRatesData)
	if err != nil {
		return
	}

	startDateRate = ethRatesData[0].High
	endDateRate = ethRatesData[len(ethRatesData)-1].High
	maxEthRate = getMaxRate(ethRatesData)
	return
}

func getMaxRate(arr []types.PoloneixData) (max float64) {
	for _, txn := range arr {
		if txn.High > max {
			max = txn.High
		}
	}
	return
}
